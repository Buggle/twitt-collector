from django.db import models
from django.contrib.auth.models import User


class List(models.Model):
    title = models.CharField(max_length=128)
    user = models.ForeignKey(User)
    date = models.DateTimeField()

    def __unicode__(self):  # For Python 2, use __str__ on Python 3
        return self.title


class Link(models.Model):
    url = models.TextField()
    list = models.ForeignKey(List)
