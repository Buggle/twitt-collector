from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render


def main(request):
    lists = []
    if request.user.is_authenticated():
        lists = request.user.list_set.all()
    return render(request, 'hamidOnline/index.html', {'lists': lists})
